#First arg is the file with the list of stop words
#Second arg is the csv with the parts of speech
#Third arg is the file with all of the titles, descriptions, etc.

import gensim
import re
import csv
import sys
import ast

googleLoc = '../model/GoogleNews-vectors-negative300.bin'

#Make sure that googleLoc points to the right file. It's 3GB, so I'm not putting it in the repo.

stopWords = []
with open(sys.argv[1], 'rb') as stopFile:
  stopWords = stopFile.readlines()

  stopFile.close()

stopWords = [ s.strip() for s in stopWords]
stopWords = stopWords + [s.capitalize() for s in stopWords]
#print stopWords

use_POS = []
with open(sys.argv[2], 'rb') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')

  header = True
  for row in reader:
    if not header:
	if row[2] == '1':
		use_POS.append(row[0])
    else:
      header = False

  csvfile.close()

#print use_POS

model = gensim.models.KeyedVectors.load_word2vec_format(googleLoc, binary=True)  
model.init_sims(replace=True)

OnlyAscii = lambda s: re.match('^[\x00-\x7F]+$', s) != None

with open(sys.argv[3], 'rb') as outFile:

  while True:
	line1 = outFile.readline()
	line2 = outFile.readline()
	
	if not line2: break

	print line1.strip()

	pos_tags = ast.literal_eval(line2)
	

	words = []

	for t in pos_tags:
		if t[0] in stopWords:
			continue
		if t[1] not in use_POS:
			continue
		if not OnlyAscii(t[0]):
			continue
		if t[0] not in model.wv.vocab:
			continue
		
		words.append(t[0])

	words = [word.encode() for word in words]
	print words

  outFile.close()
