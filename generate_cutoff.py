import sys
import ast

cuttoff_recognition = .55
cuttoff_between = 1

with open(sys.argv[1], 'rb') as outFile:
  print "[]"
  while True:
	title = outFile.readline()
	capabilities_list = outFile.readline()
	
	if not capabilities_list: break

	#print title.strip()

	capabilities = ast.literal_eval(capabilities_list)
	
	prev = (0, 0)
	guessed_required = []
	for c in capabilities:
		if float(c[1]) < cuttoff_recognition :
			break
		if prev == (0, 0):
			guessed_required.append(c[0])
		elif float(prev[1]) - float(c[1]) > cuttoff_between:
			break
		else:
			guessed_required.append(c[0])
		prev = c

	
	print guessed_required


	not_required = len(capabilities) - len(guessed_required)
	
	guessed_not_required = []
	for c in capabilities[-not_required:]:
		guessed_not_required.append(c[0])

	#print guessed_not_required

