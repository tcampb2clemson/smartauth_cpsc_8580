#First arg is the csv with the list of capabilities
#Second arg is the file with all of the words to use

#Make sure that googleLoc points to the right file. It's over 1GB, so I'm not putting it in the repo.

import gensim
import logging
import csv
import sys
import ast

file1 = '../capability_keywords.csv'
file2 = './words_all.txt'

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
model = gensim.models.KeyedVectors.load_word2vec_format('../model/GoogleNews-vectors-negative300.bin', binary=True)  
model.init_sims(replace=True)

capabilities = []

with open(file1, 'rb') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  
  header = True
  values = []
  for row in reader:
    if header == False:
	cap_words = filter(None, row[1].split(' '))
	cap_words2 = []
	for w in cap_words:
		if not w[0].isupper():
			if w.capitalize() in model.wv.vocab:
				cap_words2.append(w.capitalize())
	cap_words = cap_words+cap_words2
	
	capabilities.append((row[0], cap_words))
	
    else:
	  header = False
  
  csvfile.close()


with open(file2, 'rb') as outFile:

  while True:
	line1 = outFile.readline()
	line2 = outFile.readline()
	
	if not line2: break
	
	app_words = ast.literal_eval(line2)

	values = []
	for c in capabilities:
		values.append((c[0], model.wv.n_similarity(c[1], app_words)))
	#for c in capabilities:
	#	total = 0
	#	for w in app_words:
	#		total = total + model.wv.n_similarity([w], c[1])
	#	values.append((c[0], total/len(app_words)))
	
	
	values.sort(key=lambda tup: tup[1], reverse=True)
	
	print line1.strip()
	print values
	
  outFile.close()
	
	
	
