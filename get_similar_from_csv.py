# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 11:39:34 2016

@author: Chris
"""

# Explore Google's huge Word2Vec model.

import gensim
import logging
import csv
import sys
with open(sys.argv[1], 'rb') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
	

# Logging code taken from http://rare-technologies.com/word2vec-tutorial/
  logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# Load Google's pre-trained Word2Vec model.
  model = gensim.models.KeyedVectors.load_word2vec_format('../model/GoogleNews-vectors-negative300.bin', binary=True)  
  model.init_sims(replace=True)

# Does the model include stop words?
#print("Does it include the stop words like \'a\', \'and\', \'the\'? %d %d %d" % ('a' in model.vocab, 'and' in model.vocab, 'the' in model.vocab))

# Retrieve the entire list of "words" from the Google Word2Vec model, and write
# these out to text files so we can peruse them.
#vocab = model.vocab.keys()

  fileNum = 1

#wordsInVocab = len(vocab)
#wordsPerFile = int(100E3)
  header = True
  values = []
  for row in reader:
    if header == False:
	#print
    	#print row[0]
    	#print '; '.join(row)
	p = filter(None, row[1].split(' '))
	p2 = []
	for w in p:
		if not w[0].isupper():
			if w.capitalize() in model.wv.vocab:
				p2.append(w.capitalize())
	p = p+p2
	#print >> sys.stderr, p
    	#print(model.wv.most_similar(positive=p))
        #s = ['app', 'designed', 'turn', 'on', 'coffee', 'machine', 'taking', 'shower']
	#s = ['turn', 'on', 'light', 'dimmer', 'room', 'dark', 'illuminance', 'threshold', 'sunset', 'sunrise', 'off', 'minute', 'brightness', 'movement']

	#Smart Nightlight - good
	#s = ['Turns', 'on', 'lights', 'dark', 'motion', 'detected', 'Turns', 'lights', 'off', 'becomes', 'light', 'time', 'after', 'motion', 'ceases']

	#Smart Security
	#of not in dictionary
	#not good- the word water messes things up
	#maybe don't use 'you'; gives thermostat for som
	#s = ['Alerts', 'you', 'intruders', 'you', 'got', 'up', 'for', 'glass', 'water', 'in', 'middle', 'night']
	#s = ['Alerts', 'intruders']

	#Smart Care - Detect Motion
	#s = ['Monitors', 'motion', 'sensors', 'bedroom', 'bathroom', 'during', 'night', 'detects', 'occupant', 'return', 'bathroom', 'after', 'specified', 'period', 'time']

	#Speaker Control
	#tedious to get everything
	#Adding Motion and other capitalized words improved the position of motionSensor
	#Same with smokeDetector, added detector
	#Added press and pressed to button
	#Added acceleration to accelerationSensor
	s = ['Play', 'sound', 'custom', 'message', 'through', 'Speaker', 'mode', 'changes', 'other', 'events', 'occur', 'When', 'Motion', 'Here', 'Contact', 'Opens', 'Contact', 'Closes', 'Acceleration', 'Detected', 'Switch', 'Turned', 'On', 'Switch', 'Turned', 'Off', 'Arrival', 'Departure', 'Smoke', 'Detected', 'Water', 'Sensor', 'Wet', 'Button', 'Press', 'Speaker', 'Music', 'Player']
	total = 0
	for w in s:
		#print w
		total = total + model.wv.n_similarity([w], p)
	#print total/len(s)

	values.append((row[0], total/len(s)))
    else:
	header = False

  values.sort(key=lambda tup: tup[1])
  print values
# Write out the words in 100k chunks.
#for wordIndex in range(0, wordsInVocab, wordsPerFile):
    # Write out the chunk to a numbered text file.    
#    with open("vocabulary/vocabulary_%.2d.txt" % fileNum, 'w') as f:
        # For each word in the current chunk...        
#        for i in range(wordIndex, wordIndex + wordsPerFile):
            # Write it out and escape any unicode characters.            
#            f.write(vocab[i].encode('UTF-8') + '\n')
    
#    fileNum += 1
