# change this path if necessary
path_to_smartapps_dir = "../SmartThingsPublic/smartapps/"

########################################################################

import os
import re
import csv

num_caps_found = 0


class app_info:
	def __init__(self, groovy_file_name):
		with open(groovy_file_name, 'r') as myfile:
			data = myfile.read()
			
			self.name = self.get_basic("name", data)
			self.description = self.get_basic("description", data) \
			+ ". " + \
			". ".join( self.get_section_labels(data) ) + ". " + \
			". ".join( self.get_titles_following_capabilities(data) )
			
			self.code_capabilities = self.get_code_capabilities(data) 
			self.description_capabilities = set() # plug in NLP
			
	def get_section_labels(self, data):
		# gets all section labels (even sections that don't have capabilities)
		return re.findall(r'section\("(.*?)"\)', data)
		
	def get_titles_following_capabilities(self, data):
		# get titles that follow capabilities
		ret =  re.findall(r'"capability\..*?title\s*?:\s*?"(.*?)"', data)
		# print(self.name)
		# print(ret)
		return ret
		
	def get_basic(self, key, data):
		"""
		key: string
		data: string
		gets the value for the first instance of key in data, where value is the double-quoted string following the key like this
		<key><1 or 0 spaces>:<any number of spaces>"<value>"
		eg
		name: "Foo"
		name : "Foo"
		name :"Foo"
		"""
		wsp = "\s*"# whitespace
		return re.search(key+r'\s*:\s*"(.*?)"', data).group(1)
		
	def get_code_capabilities(self, data):
		ret = re.findall(r'"capability\.(.*?)"', data)
		global num_caps_found
		num_caps_found = num_caps_found + len(ret)
		return set(ret)

def print_output(all_apps):
	global num_caps_found
	for i in all_apps:
		#print "App name:"
		#print i.name 
		#print "Description: "
		#print i.description 
		#print "Capabilities used in the code: "
		#print i.code_capabilities
		#print
		writer.writerow([i.name, " ".join(i.code_capabilities)])
	print "num of apps found " + str(len(all_apps))
	print "num of capabilities found in the code " + str(num_caps_found)
	
if __name__ == "__main__":
	
	all_apps = []	 # or list of objects, each having a name, description, and set of capabilities
	for root, dirs, files in os.walk(path_to_smartapps_dir):
		# walk goes to each directory, listing it's name, it's subdirs, and its files
		for file in files:
			if file.endswith(".groovy"):
				all_apps.append( app_info( os.path.join(root, file) ) )

	for app in all_apps:
		app.differences = app.code_capabilities - app.description_capabilities # capabilities code asks for but description does not require
		# assert(app.differences == app.code_capabilities)

	with open("./testing/actual_capabilities.csv", 'wb') as fout:
		writer = csv.writer(fout, delimiter=',')
		writer.writerow(["App Name", "Capabilities"])
		print_output(all_apps)


	

