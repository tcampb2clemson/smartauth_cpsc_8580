# change this path if necessary
########################################################################

import os
import re

path_to_smartapps_dir = "../SmartThingsPublic/smartapps/"
os.environ["STANFORD_MODELS"] = "../stanford-postagger-full-2018-02-27/models"
os.environ["CLASSPATH"] = "../stanford-postagger-full-2018-02-27/stanford-postagger.jar"

from nltk.tag import StanfordPOSTagger
st = StanfordPOSTagger('english-bidirectional-distsim.tagger')

content = []
file_nums = []
tags = []
file_num = 0

class app_info:
	

	def __init__(self, groovy_file_name, num):
		with open(groovy_file_name, 'r') as myfile:
			data = myfile.read()
			
			self.name = self.get_basic("name", data)
			self.description = self.get_basic("description", data)
			content.append(re.split(' |\.|,|;|\(|\)', self.description))
			file_nums.append(num)
			
			labels = self.get_section_labels(data)
			for l in labels:
				content.append(re.split(' |\.|,|;|\(|\)', l))
				file_nums.append(num)
				
			titles = self.get_titles_following_capabilities(data)
			for t in titles:
				content.append(re.split(' |\.|,|;|\(|\)', t))
				file_nums.append(num)
			
			
	def get_section_labels(self, data):
		# gets all section labels (even sections that don't have capabilities)
		return re.findall(r'section\("(.*?)"\)', data)
		
	def get_titles_following_capabilities(self, data):
		# get titles that follow capabilities
		ret =  re.findall(r'"capability\..*?title\s*?:\s*?"(.*?)"', data)
		return ret
		
	def get_basic(self, key, data):
		"""
		key: string
		data: string
		gets the value for the first instance of key in data, where value is the double-quoted string following the key like this
		<key><1 or 0 spaces>:<any number of spaces>"<value>"
		eg
		name: "Foo"
		name : "Foo"
		name :"Foo"
		"""
		wsp = "\s*"# whitespace
		return re.search(key+r'\s*:\s*"(.*?)"', data).group(1)


def print_output(all_apps):
	global num_caps_found
	count = 0
	for i in all_apps:
		#print "App name:"
		print i.name 
		#print "Description: "
		#print i.description
		print tags[count]
		count = count + 1
	
if __name__ == "__main__":
	 
	all_apps = []	 # or list of objects, each having a name, description, and set of capabilities
	for root, dirs, files in os.walk(path_to_smartapps_dir):
		# walk goes to each directory, listing it's name, it's subdirs, and its files
		for file in files:
			if file.endswith(".groovy"):
				all_apps.append( app_info( os.path.join(root, file), file_num ) )
				file_num = file_num + 1


	output = st.tag_sents(content)
	#print(output)
	app_sentences = []
	count = 0
	index = 0
	for c in content:
		if file_nums[index] == count:
			app_sentences = app_sentences + output[index]
		else:
			tags.append(app_sentences)
			
			app_sentences = output[index]
			
			count = count + 1
		index = index + 1
	tags.append(app_sentences)
	
	print_output(all_apps)


	

