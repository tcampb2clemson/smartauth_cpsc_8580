# change this path if necessary
path_to_smartapps_dir = "../SmartThingsPublic/smartapps/"

########################################################################

import os
import re

os.environ["STANFORD_MODELS"] = "../stanford-postagger-full-2018-02-27/models"
os.environ["CLASSPATH"] = "../stanford-postagger-full-2018-02-27/stanford-postagger.jar"

from nltk.tag import StanfordPOSTagger
st = StanfordPOSTagger('english-bidirectional-distsim.tagger')

descriptions = []
tags = []

num_caps_found = 0
class app_info:
	def __init__(self, groovy_file_name):
		with open(groovy_file_name, 'r') as myfile:
			data = myfile.read()
			
			self.name = self.get_basic("name", data)
			self.description = self.get_basic("description", data)
			descriptions.append(re.split(' |\.|,|;|\(|\)', self.description))
			#self.tags = self.get_tags()
			#+ ". " + \
			#					". ".join( self.get_section_labels(data) ) + ". " + \
			#					". ".join( self.get_titles_following_capabilities(data) )
			
			
	def get_section_labels(self, data):
		# gets all section labels (even sections that don't have capabilities)
		return re.findall(r'section\("(.*?)"\)', data)
		
	def get_titles_following_capabilities(self, data):
		# get titles that follow capabilities
		ret =  re.findall(r'"capability\..*?title\s*?:\s*?"(.*?)"', data)
		return ret
		
	def get_basic(self, key, data):
		"""
		key: string
		data: string
		gets the value for the first instance of key in data, where value is the double-quoted string following the key like this
		<key><1 or 0 spaces>:<any number of spaces>"<value>"
		eg
		name: "Foo"
		name : "Foo"
		name :"Foo"
		"""
		wsp = "\s*"# whitespace
		return re.search(key+r'\s*:\s*"(.*?)"', data).group(1)


def print_output(all_apps):
	global num_caps_found
	count = 0
	for i in all_apps:
		#print "App name:"
		print i.name 
		#print "Description: "
		#print i.description
		print tags[count]
		count = count + 1
	
if __name__ == "__main__":
	 
	all_apps = []	 # or list of objects, each having a name, description, and set of capabilities
	for root, dirs, files in os.walk(path_to_smartapps_dir):
		# walk goes to each directory, listing it's name, it's subdirs, and its files
		for file in files:
			if file.endswith(".groovy"):
				all_apps.append( app_info( os.path.join(root, file) ) )


	tags = st.tag_sents(descriptions)

	print_output(all_apps)


	

