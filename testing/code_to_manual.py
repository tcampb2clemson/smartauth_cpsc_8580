

import csv
import ast

manual_f = "description_capabilities_manual.csv"
code_f = "actual_capabilities.csv"
out_file= "manual_to_code.csv"


with open(manual_f, 'rb') as manual_file, open(code_f, 'rb') as code_file, open(out_file, 'wb') as fout:
	manual_apps = csv.reader(manual_file, delimiter=",")
	code_apps = csv.reader(code_file, delimiter=",")
	out = csv.writer(fout, delimiter=",")
	out.writerow(["Name", "Manual Caps", "Code Caps", "Overpriviledged Caps"])
	
	for m, c in zip(manual_apps, code_apps)[1:]:
		name = c[0]
		man_caps = set(m[1].split())
		code_caps = set(i.lower() for i in c[1].split())
		
		if ("all" in man_caps or "externalapi" in man_caps):
			disallowed_caps = set()
		else:
			disallowed_caps = code_caps - man_caps
		
		out.writerow([
				name,
				" ".join(man_caps),
				" ".join(code_caps),
				" ".join(disallowed_caps)
				])
	
	
		
	