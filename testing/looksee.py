"""
	compares the capabilities NLP finds in the description to those manual analysis find in it
"""

# for each app, get the NLP result and the manual analysis result, and compare them
# false positives, NLP said should need, but manual did not . i.e. in set NLP but not set manual ie NLP - manual
# false negatives, NLP did not say should need, but manual says does need i.e. manual - NLP

import csv


import sys
import ast

cuttoff_recognition = 0.50
cuttoff_between = 0.05

with open("../guess.txt", 'rb') as guesses,  open("description_capabilities_manual.csv", 'rb') as actual_file:

	actual = csv.reader(actual_file, delimiter=',') # the capabilities manually found for each app, 1 app per row
	header = True
	for manual_app in actual: # for a given app
		if header:
			header = False
			continue
		
		name = manual_app[0]
		manual_caps = manual_app[1].split()
		title = guesses.readline().strip() # to skip it
		guess_caps = guesses.readline()
		if not guess_caps: break
		guess_caps = ast.literal_eval(guess_caps)
		not_guessed_caps = ast.literal_eval( guesses.readline() )
		
		print title
		print( "Actual, manual Capabilities", manual_caps)
		print ("Guess Capabilities", guess_caps)
		print not_guessed_caps
	
	
	

	
