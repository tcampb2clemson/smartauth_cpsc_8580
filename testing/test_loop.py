from __future__ import division

"""
	compares the capabilities NLP finds in the description to those manual analysis find in it
"""

# for each app, get the NLP result and the manual analysis result, and compare them
# false positives, NLP said should need, but manual did not . i.e. in set NLP but not set manual ie NLP - manual
# false negatives, NLP did not say should need, but manual says does need i.e. manual - NLP

import csv

# manual description capabilities compared to guessed capabilities
in_file = "description_capabilities_manual.csv"
out_file= "nlp_vs_manual_omitting_externalapi_and_all.csv"
column_label = "Manual Capabilities"

# actual code capabilities compared to guessed capabilities
#in_file = "actual_capabilities.csv"
#out_file = "nlp_vs_code.csv"
#column_label = "Actual/Code Capabilities"




import sys
import ast

cutoff_recognition = [0.05, 0.1, 0.15, 0.2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7]
cutoff_between = [1, 0.5, .375, .25, .20, .175, .15, .125, .1, .09, .08, .07, .06, .05]

guesses = []

for r in cutoff_recognition:
	for b in cutoff_between:
		with open(sys.argv[1], 'rb') as outFile:
			guessed_required = [[]]
			while True:
				title = outFile.readline()
				capabilities_list = outFile.readline()
			
				if not capabilities_list: break

				capabilities = ast.literal_eval(capabilities_list)
			
				prev = (0, 0)
				for c in capabilities:
					if float(c[1]) < r :
						break
					if prev == (0, 0):
						guessed_required.append(c[0])
					elif float(prev[1]) - float(c[1]) > b:
						break
					else:
						guessed_required.append(c[0])
					prev = c

			guesses.append(guessed_required)
			outFile.close()

index = 0
for g in guesses:
	with open(in_file, 'rb') as fin:
		num_apps_tested = 0 # count them as we go through
		num_apps_with_FN =0
		
		#print g
		manual_apps = csv.reader(fin, delimiter=',') # the capabilities manually found for each app, 1 app per row
		#writer = csv.writer(fout, delimiter=',')
		nlp_apps = g
		#print g
		
		
		#writer.writerow(["App Name", "False Positives", "False Positive Count", "False Negatives", "False Negative Count", column_label , "NLP Capabilities", "Is Overpriviledged"])
		false_pos_avg = 0
		false_neg_avg = 0
		true_pos_avg = 0
		true_neg_avg = 0
		for manual_app, nlp_app in zip(manual_apps, nlp_apps)[1:]: # for a given app
			name = manual_app[0]
			manual_capabilities = set(manual_app[1].split())
			nlp_capabilities = set([w.lower() for w in nlp_app]) # todo #todo: make lowercase

			if ("externalapi" in manual_capabilities or "all" in manual_capabilities ):

				false_pos = set()
				false_neg = set()
				true_pos_count = 0
				true_neg_count = 0
			
			else:
				num_apps_tested += 1
				false_pos = nlp_capabilities - manual_capabilities
				false_neg = manual_capabilities - nlp_capabilities
				# print(name)
				# print(manual_capabilities)
				# print(nlp_capabilities)
				true_pos_count = len( nlp_capabilities & manual_capabilities )
				# print(true_pos_count)

				true_neg_count = 61 - true_pos_count - len(false_pos) - len(false_neg)

			
			false_pos_avg += len(false_pos)
			false_neg_avg += len(false_neg)
			true_pos_avg +=  true_pos_count
			true_neg_avg +=  true_neg_count
			
			is_overpriv = False
			if (false_neg):
				is_overpriv = True
				num_apps_with_FN += 1
			
			#writer.writerow([name, 
			#	" ".join(false_pos),
			#	len(false_pos), 
			#	" ".join(false_neg),
			#	len(false_neg), 
			#	" ".join(manual_capabilities),
			#	" ".join(nlp_capabilities),
			#	is_overpriv
			#])
		
		false_pos_avg = false_pos_avg/ num_apps_tested
		false_neg_avg = false_neg_avg / num_apps_tested
		true_pos_avg = true_pos_avg / num_apps_tested
		true_neg_avg = true_neg_avg / (num_apps_tested)
		print("cutoff_recognition", cutoff_recognition[index//len(cutoff_recognition)])
		print("cutoff_between", cutoff_between[index % len(cutoff_between)])
		print(false_pos_avg, false_neg_avg, true_pos_avg, true_neg_avg)
		print("False Pos Avg, False Neg Avg, True Pos Avg, True Neg Avg")
		print("num apps tested", num_apps_tested)
		print("num apps with FN", num_apps_with_FN)
		print("Accuracy", (true_pos_avg+true_neg_avg)/num_apps_tested)
		if (true_pos_avg+false_pos_avg) == 0:
			print("Precision", "0")
		else:
			print("Precision", true_pos_avg/(true_pos_avg+false_pos_avg))
		print
		index = index + 1
		fin.close()
		#writer.writerow([])
		#writer.writerow(["False Pos Avg", "False Neg Avg", "True Pos Avg", "True Neg Avg"])
		#writer.writerow([false_pos_avg, false_neg_avg, true_pos_avg, true_neg_avg])
		#writer.writerow(["number of apps with FNs (is overpriv app count for nlp_vs_code)"])
		#writer.writerow([num_apps_with_FN])

	

	
