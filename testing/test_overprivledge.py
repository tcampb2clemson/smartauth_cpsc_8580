from __future__ import division

"""
	compares the capabilities NLP finds in the description to those manual analysis find in it
"""

# for each app, get the NLP result and the manual analysis result, and compare them
# false positives, NLP said should need, but manual did not . i.e. in set NLP but not set manual ie NLP - manual
# false negatives, NLP did not say should need, but manual says does need i.e. manual - NLP

import csv
import ast

num_apps_tested = 184

# manual - description_capabilities_manual.csv
# acutal - actual_capabilities.csv
with open("actual_capabilities.csv", 'rb') as fin, open("guess.txt", 'rb') as fin2, open("false_pos_and_neg_actual.csv", 'wb') as fout:
	
	manual_apps = csv.reader(fin, delimiter=',') # the capabilities manually found for each app, 1 app per row
	writer = csv.writer(fout, delimiter=',')
	nlp_apps = [ast.literal_eval(line) for line in fin2.readlines()]
	
	
	writer.writerow(["App Name", "False Positives", "False Positive Count", "False Negatives", "False Negative Count", "Actual/Code Capabilities", "NLP Capabilities"])
	false_pos_avg = 0
	false_neg_avg = 0
	true_pos_avg = 0
	true_neg_avg = 0
	for manual_app, nlp_app in zip(manual_apps, nlp_apps)[1:]: # for a given app
		name = manual_app[0]
		manual_capabilties = set(manual_app[1].split())
		nlp_capabilities = set([w.lower() for w in nlp_app]) # todo #todo: make lowercase
		if (manual_capabilties == set("externalapi") ):
			false_pos = set()
			false_neg = set()
			true_pos_count = 0
			true_neg_count = 0
		
		else:
			false_pos = nlp_capabilities - manual_capabilties
			false_neg = manual_capabilties - nlp_capabilities
			# print(name)
			# print(manual_capabilties)
			# print(nlp_capabilities)
			true_pos_count = len( nlp_capabilities & manual_capabilties )
			# print(true_pos_count)
			true_neg_count = 61 - true_pos_count - len(false_pos) - len(false_neg)
		
		false_pos_avg += len(false_pos)
		false_neg_avg += len(false_neg)
		true_pos_avg +=  true_pos_count
		true_neg_avg +=  true_neg_count
		
		writer.writerow([name, 
			" ".join(false_pos),
			len(false_pos), 
			" ".join(false_neg),
			len(false_neg), 
			" ".join(manual_capabilties),
			" ".join(nlp_capabilities),
		])
	
	false_pos_avg = false_pos_avg/ num_apps_tested
	false_neg_avg = false_neg_avg / num_apps_tested
	true_pos_avg = true_pos_avg / num_apps_tested
	true_neg_avg = true_neg_avg / num_apps_tested
	print(false_pos_avg, false_neg_avg, true_pos_avg, true_neg_avg)

	writer.writerow([])
	writer.writerow(["False Pos Avg", "False Neg Avg", "True Pos Avg", "True Neg Avg"])
	writer.writerow([false_pos_avg, false_neg_avg, true_pos_avg, true_neg_avg])
	

	
